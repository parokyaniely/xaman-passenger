package com.xaman.passenger;

import android.databinding.DataBindingUtil;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.xaman.passenger.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
//        binding.tablayout.addTab(binding.tablayout.newTab().setText("Bus List"));
        binding.tablayout.addTab(binding.tablayout.newTab().setText("Map"));
        binding.tablayout.setupWithViewPager(binding.pager);
        binding.pager.setAdapter(new MainPagerAdapter(getSupportFragmentManager()));
        binding.tablayout.getTabAt(0).setText("Bus List");
        binding.tablayout.getTabAt(1).setText("Map");
    }
}
