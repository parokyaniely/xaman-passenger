package com.xaman.passenger;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.xaman.passenger.buslist.BusListFragment;
import com.xaman.passenger.map.MapFragment;

import java.util.ArrayList;

/**
 * Created by monica on 12/29/17.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {
    FragmentManager supportFragmentManager;
    ArrayList<Fragment> fragments;

    public MainPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
        this.supportFragmentManager = supportFragmentManager;
        fragments = new ArrayList<>();
        initFragments();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    private void initFragments() {
        fragments.add(new BusListFragment());
        fragments.add(new MapFragment());
    }
}
