package com.xaman.passenger.map;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.xaman.passenger.R;
import com.xaman.passenger.databinding.FragmentMapBinding;
import com.xaman.passenger.model.BusListStatus;

/**
 * Created by monica on 12/29/17.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback, OnSuccessListener<Location>, BusListView {
    private FragmentMapBinding binding;
    private GoogleMap googleMap;
    private FusedLocationProviderClient fusedLocationClient;
    private BusListPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.map.onCreate(savedInstanceState);
        initMap();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.map.onResume();
    }

    private void initMap() {
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        binding.map.getMapAsync(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.map.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.map.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.map.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        this.googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map_style));
        this.googleMap.setMyLocationEnabled(true);
        /*fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        fusedLocationClient.getLastLocation().addOnSuccessListener(this);*/
        LocationManager locationManager = ((LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE));
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        onSuccess(location);
        lookForBuses();
    }

    @Override
    public void onSuccess(Location location) {
/*        if (location != null) {
//            LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            LatLng currentLocation = new LatLng(14.581079, 121.053478);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLocation).zoom(14).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }*/
        LatLng currentLocation = new LatLng(14.581079, 121.053478);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLocation).zoom(14).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void busListUpdated(BusListStatus status) {
        //TODO: Split coordinates
        String[] latlong = new String[2];
        double latitude = 0.0;
        double longitude = 0.0;

        if (status.getCherbus().getLatlong() != null) {
            latlong = status.getCherbus().getLatlong().split(",");
            latitude = Double.parseDouble(latlong[0]);
            longitude = Double.parseDouble(latlong[1]);
            googleMap.addMarker(new MarkerOptions()
                    .title(status.getCherbus().getName())
                    .position(new LatLng(latitude, longitude)));
        }

        if (status.getFivestarbus().getLatlong() != null) {
            latlong = status.getFivestarbus().getLatlong().split(",");
            latitude = Double.parseDouble(latlong[0]);
            longitude = Double.parseDouble(latlong[1]);
            googleMap.addMarker(new MarkerOptions()
                    .title(status.getFivestarbus().getName())
                    .position(new LatLng(latitude, longitude)));
        }

        if (status.getJayross().getLatlong() != null) {
            latlong = status.getJayross().getLatlong().split(",");
            latitude = Double.parseDouble(latlong[0]);
            longitude = Double.parseDouble(latlong[1]);
            googleMap.addMarker(new MarkerOptions()
                    .title(status.getJayross().getName() != null ? status.getJayross().getName() : "Jayross Tours Baclaran - Tungko via Ayala Commonwealth")
                    .position(new LatLng(latitude, longitude)));
        }

        if (status.getKellen().getLatlong() != null) {
            latlong = status.getKellen().getLatlong().split(",");
            latitude = Double.parseDouble(latlong[0]);
            longitude = Double.parseDouble(latlong[1]);
            googleMap.addMarker(new MarkerOptions()
                    .title(status.getKellen().getName()!= null ? status.getKellen().getName() : "Kellen Bus Baclaran - Sapang Palay via Cubao Commonwealth")
                    .position(new LatLng(latitude, longitude)));
        }
        if (status.getPartas().getLatlong() != null) {
            latlong = status.getPartas().getLatlong().split(",");
            latitude = Double.parseDouble(latlong[0]);
            longitude = Double.parseDouble(latlong[1]);
            googleMap.addMarker(new MarkerOptions()
                    .title(status.getPartas().getName() != null ? status.getPartas().getName() : "Manila to Laoag via SCTEX vv")
                    .position(new LatLng(latitude, longitude)));
        }

        if (status.getVictoryliner().getLatlong() != null) {
            latlong = status.getVictoryliner().getLatlong().split(",");
            latitude = Double.parseDouble(latlong[0]);
            longitude = Double.parseDouble(latlong[1]);
            googleMap.addMarker(new MarkerOptions()
                    .title(status.getVictoryliner().getName())
                    .position(new LatLng(latitude, longitude)));
        }

    }

    @Override
    public void busListFailed(String error) {

    }



    //TODO: look for current location
    private void showLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationClient.getLastLocation();
    }

    private void lookForBuses() {
        presenter = new BusListPresenter(this);
        presenter.getBuses();
    }
}
