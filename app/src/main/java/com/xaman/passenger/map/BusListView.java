package com.xaman.passenger.map;

import android.content.Context;

import com.xaman.passenger.model.BusListStatus;

/**
 * Created by monica on 12/31/17.
 */

public interface BusListView {
    void busListUpdated(BusListStatus status);
    void busListFailed(String error);
    Context getContext();
}
