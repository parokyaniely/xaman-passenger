package com.xaman.passenger.map;

import com.xaman.passenger.model.BusListStatus;
import com.xaman.passenger.network.XamanPassengerRetrofit;
import com.xaman.passenger.network.XamanService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by monica on 12/31/17.
 */

public class BusListPresenter {
    private final XamanService xamanService;
    BusListView view;

    public BusListPresenter(BusListView view) {
        this.view = view;
        XamanPassengerRetrofit retrofit = new XamanPassengerRetrofit();
        xamanService = retrofit.getXamanRetrofit().create(XamanService.class);
    }

    public void getBuses() {
        xamanService.updateLocationStatus().enqueue(new Callback<BusListStatus>() {
            @Override
            public void onResponse(Call<BusListStatus> call, Response<BusListStatus> response) {
                if (response.code() == 200) {
                    view.busListUpdated(response.body());
                } else {
                    view.busListFailed(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<BusListStatus> call, Throwable t) {
                view.busListFailed("error");
            }
        });
    }
}
