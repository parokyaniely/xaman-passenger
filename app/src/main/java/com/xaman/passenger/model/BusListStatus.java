package com.xaman.passenger.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by monica on 12/31/17.
 */

public class BusListStatus implements Parcelable {
    @SerializedName("cherbus")
    @Expose
    public Bus cherbus;
    @SerializedName("fivestarbus")
    @Expose
    public Bus fivestarbus;
    @SerializedName("jayross")
    @Expose
    public Bus jayross;
    @SerializedName("kellen")
    @Expose
    public Bus kellen;
    @SerializedName("partas")
    @Expose
    public Bus partas;
    @SerializedName("victoryliner")
    @Expose
    public Bus victoryliner;

    protected BusListStatus(Parcel in) {
        cherbus = in.readParcelable(Bus.class.getClassLoader());
        fivestarbus = in.readParcelable(Bus.class.getClassLoader());
        jayross = in.readParcelable(Bus.class.getClassLoader());
        kellen = in.readParcelable(Bus.class.getClassLoader());
        partas = in.readParcelable(Bus.class.getClassLoader());
        victoryliner = in.readParcelable(Bus.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(cherbus, flags);
        dest.writeParcelable(fivestarbus, flags);
        dest.writeParcelable(jayross, flags);
        dest.writeParcelable(kellen, flags);
        dest.writeParcelable(partas, flags);
        dest.writeParcelable(victoryliner, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BusListStatus> CREATOR = new Creator<BusListStatus>() {
        @Override
        public BusListStatus createFromParcel(Parcel in) {
            return new BusListStatus(in);
        }

        @Override
        public BusListStatus[] newArray(int size) {
            return new BusListStatus[size];
        }
    };

    public Bus getCherbus() {
        return cherbus;
    }

    public void setCherbus(Bus cherbus) {
        this.cherbus = cherbus;
    }

    public Bus getFivestarbus() {
        return fivestarbus;
    }

    public void setFivestarbus(Bus fivestarbus) {
        this.fivestarbus = fivestarbus;
    }

    public Bus getJayross() {
        return jayross;
    }

    public void setJayross(Bus jayross) {
        this.jayross = jayross;
    }

    public Bus getKellen() {
        return kellen;
    }

    public void setKellen(Bus kellen) {
        this.kellen = kellen;
    }

    public Bus getPartas() {
        return partas;
    }

    public void setPartas(Bus partas) {
        this.partas = partas;
    }

    public Bus getVictoryliner() {
        return victoryliner;
    }

    public void setVictoryliner(Bus victoryliner) {
        this.victoryliner = victoryliner;
    }
}
