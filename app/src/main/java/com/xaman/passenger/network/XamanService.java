package com.xaman.passenger.network;

import com.xaman.passenger.model.BusListStatus;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by monica on 12/31/17.
 */

public interface XamanService {
    @GET("buses")
    Call<BusListStatus> updateLocationStatus();
}
