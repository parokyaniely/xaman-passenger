package com.xaman.passenger.buslist;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.xaman.passenger.databinding.ItemBusBinding;

/**
 * Created by monica on 12/29/17.
 */

public class BusListViewHolder extends RecyclerView.ViewHolder {
    public BusListViewHolder(ItemBusBinding binding) {
        super(binding.getRoot());
    }
    public void bind() {

    }
}
