package com.xaman.passenger.buslist;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xaman.passenger.R;
import com.xaman.passenger.databinding.FragmentBusListBinding;

/**
 * Created by monica on 12/28/17.
 */

public class BusListFragment extends Fragment {
    FragmentBusListBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bus_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRv();
    }

    private void initRv() {
        binding.rvBuses.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvBuses.setAdapter(new BusListAdapter(getContext()));

    }
}
