package com.xaman.passenger.buslist;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.xaman.passenger.R;
import com.xaman.passenger.databinding.ItemBusBinding;
import com.xaman.passenger.databinding.ItemStationBinding;

import java.util.ArrayList;

/**
 * Created by monica on 12/29/17.
 */

public class BusListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    final int STATION = 1;
    final int BUS = 2;
    private ArrayList<BusList> busesList;
    private Context context;

    public BusListAdapter(Context context) {
        this.context = context;
        busesList = new ArrayList<>();
        initSampleBusesList();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (viewType == STATION) {
            ItemStationBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_station, parent, false);
            return new BusStationViewHolder(binding);
        } else if (viewType == BUS) {
            ItemBusBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_bus, parent, false);
            return new BusListViewHolder(binding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case BUS:
                ((BusListViewHolder) holder).bind();
                break;
            case STATION:
                ((BusStationViewHolder) holder).bind();
                break;
        }
    }

    @Override
    public int getItemCount() {
        return busesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return busesList.get(position).getType();
    }

    private void initSampleBusesList() {
        busesList.add(new BusList.Builder().type(STATION).build());
        busesList.add(new BusList.Builder().type(BUS).build());
        busesList.add(new BusList.Builder().type(BUS).build());
        busesList.add(new BusList.Builder().type(STATION).build());
        busesList.add(new BusList.Builder().type(BUS).build());
    }
}
