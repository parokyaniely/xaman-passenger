package com.xaman.passenger.buslist;

/**
 * Created by monica on 12/29/17.
 */

public class BusList {
    private int type;
    private Bus bus;
    private Station station;

    private BusList(Builder builder) {
        setType(builder.type);
        setBus(builder.bus);
        setStation(builder.station);
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public static final class Builder {
        private int type;
        private Bus bus;
        private Station station;

        public Builder() {
        }

        public Builder type(int val) {
            type = val;
            return this;
        }

        public Builder bus(Bus val) {
            bus = val;
            return this;
        }

        public Builder station(Station val) {
            station = val;
            return this;
        }

        public BusList build() {
            return new BusList(this);
        }
    }
}
