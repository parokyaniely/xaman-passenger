package com.xaman.passenger.buslist;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.xaman.passenger.databinding.ItemStationBinding;

/**
 * Created by monica on 12/29/17.
 */

public class BusStationViewHolder extends RecyclerView.ViewHolder {
    public BusStationViewHolder(ItemStationBinding binding) {
        super(binding.getRoot());
    }

    public void bind() {

    }
}